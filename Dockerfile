FROM php:7.2

RUN apt-get update -yqq && apt-get install -yqq git zlib1g-dev python && \
	docker-php-ext-install -j$(nproc) zip && \
	apt-get install curl gnupg -yq \
	&& curl -sL https://deb.nodesource.com/setup_8.x | bash \
	&& apt-get install nodejs npm -yq \
	&& curl -sS https://getcomposer.org/installer | php && \
        curl -o- -L https://yarnpkg.com/install.sh | bash && \
	curl -sS https://getcomposer.org/installer| php -- --install-dir=/usr/local/bin --filename=composer \
	&& curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit-7.5.17.phar \
	&& chmod +x /usr/local/bin/phpunit \
	&& pecl install xdebug \
	&& docker-php-ext-enable xdebug

